# What is "Chipbox" ???

Chipbox was a Linux HD digital satellite receiver. It was one of its kind in the world at 2009-2010. 

Chipbox project started in 2009 for MerihVideo company located in Istanbul, Turkey.

At that time, there was only a few Linux based satellite receivers in the world. Most of them was broadcom or Sigma based, such as Dreambox and Kathrein.

Therefore, Chipbox is may the the 3rd or 4th satellite receiver in the world with Linux OS.

# Before Chipbox

MerihVideo was a very succesful company for satellite receivers at those years in Turkey satellite receiver market. 

In those years, LCD/LED TV's mostly didn't have internal DVB tuners. Because of this, and also the popularity of cardsharing functionality, DVB market in the world was very alive.

MerihVideo was designing, manufacturing non-Linux receivers for a while. But there was an increasing demand from the customers about the functionality of Linux receivers, because Enigma project was gaining more and more traction, and introducing many new functionalities which are not available in commodity receivers.

So, MerihVideo decided to introduce Turkey's 1st Linux HD satellite receiver.

As far as I know, it is still the only Linux DVB receiver designed & developed in Turkey.

# The Development Team

The development team consisted of 5 people:

Sertaç TÜLLÜK -> Project Owner, Junior SW developer

K.C. Bae -> Experienced SW developer

K.B. Kim -> Experienced SW developer

Gökhan HÜYÜK -> Web SW developer

Gülden Erikli -> Graphics Design

# The Hardware

![](images/mainboard.jpg)

Chipbox's hardware specification was as follows:

| Feature             | Detail                                           |
|---------------------|--------------------------------------------------|
| CPU                 | CSM1203                                          |
| FLASH               | 32Mbyte                                          |
| RAM                 | 256Mbyte                                         |
| USB                 | 1x USB1.1                                        |
| Tuner               | Sharp                                            |
| HDMI                | HDMI 1.3  up to 1920x1080i                       |
| SCART               | 1x FULL                                          |
| RCA                 | Available                                        |
| Composite           | Y Pb Pr                                          |
| Ethernet            | 100Mbps                                          |
| Power               | AC220 V                                          |
| SmartCard Reader    | 1x                                               |
| CI Slot             | 1x                                               |
| Remote Control      | Philips RC5 protocol, 45 Buttons                 |
| IR Receiver         | In the front panel, controlled with an Atmel MCU |
| Front Panel Buttons | 7 buttons with Menu and Exit                     |
| Debug               | RS232 in the rear panel                          |

Front panel was looking like as follows:

![](images/front.png)

Rear panel was looked like as follows:

![](images/rear.png)

Remote Control was looked like as follows:

![](images/remote.jpg)

The PCB Layout was designed in Korea. PCB is manufactured in China. It was a very nice PCB, very durable, non-heating, robust board.

# What was CSM1203 ?

CSM1203 was a SoC developed by Celestial Semiconductor in Beijing China. 

Initially, many SoC alternatives was considered, but Celestial was chosen because Broadcom / Sigma needed huge amounts of initial order for starting.

That was one of the unfortunate fate of Chipbox, because there was not enough support from chipset vendor.

# What was the SDK delivery of Celestial Semi ?

Celestial Semi provided a very basic SDK, which consisted of following parts:

- u-boot bootloader
- linux kernel 2.6.12.5 
- Very stripped down rootfs generator
- CSAPI library for developing DVB tuner functionality
- minigui for frontend development
- Some preliminary tools

# The Software Development

The software development started in late 2009 in Istanbul with the team. 

The requirements were quite tough as it needed to exceed the expectations of customers.

The team developed mainly following parts:

- Mvapp.elf -> Main application which handled user interaction
- THTTPD -> Web Server
- Busybox
- Plugins -> Extensions developed by community to extend the functionality of the receiver

# Sales

The sales started in 2010, with a very bad feedback from customers. Main reasons are:

- UI did not looked like any other receiver in the market, it was Linux, bu it was not Enigma
- There was a lot of new features in the market, but marketing network did not know about it
- The selected initial (USD175) price was high compared to commodity receivers
- Cardsharing was based on OSCAM, but most of the people only knew CCCam. Most distribution channels didn't know how to customise the configuration files, nobody wanted to learn something new.
- ( Some time later... ) IPTV was gaining popularity, but Chipbox didn't have IPTV functionality in the beginning
- Picture quality was criticised by many customers
- Mosaic problem on TRTHD /DSMART Frequencies

# PLUGINS

One of the major selling point of Chipbox was Plugin functionality. Mainly, plugins were extension software which could be developed by community and added to Chipbox receiver software ecosystem.

Some of the major plugins :

- IPTV
- Streaming to Mobile devices
- Newspapers
- Weather
- FOREX
- Mail Sender

# Aftermath

There was a lot of critisism on TurkeyForum about the product. Sales stalled in late 2010. 

There was not enough community support for the product, and many developers left.

The plugin development was mostly done by a few people.

MerihVideo has been able to sold out all products until 2015.



